﻿
FROM microsoft/dotnet:2.2-aspnetcore-runtime
WORKDIR test
COPY ./bin/Release/netcoreapp2.2/publish .    
ENTRYPOINT ["dotnet", "test.dll"] 